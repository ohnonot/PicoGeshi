# PicoGeSHi

A [PicoCMS][p] plugin that adds [GeSHi][g] syntax highlighting to code blocks that have a language definition.
[GeSHi][g] is written in PHP and runs server-side - if you care about not pushing javascript on your site's visitors, you might like this.
It is not the newest but stable and pretty fast.

[ I have since developed the [PicoPygments][pypi2] plugin (also [here][pypi1]) because some of GeSHi's lexers haven't been updated for a very long time. ]

## Installation

There are two ways - cloning the git repository, or manually downloading the plugin.

### Git clone

- Change into your PicoCMS install's `/plugin` directory.
- Execute `git clone --recurse-submodules https://$site.org/ohnonot/PicoGeSHi`, where `$site` is either `framagit` or `notabug`.

If you follow a few simple rules, you can always `cd PicoGeSHi; git pull` to the newest version:

- Don't edit any files inside the repository.
- Copy the `picogeshi.yml` config file to your PicoCMS install's `/config` directory.
- Create custom files inside the repository that have the word `custom` in their name (see `.gitignore`) and edit `/config/picogeshi.yml` to point to those. Currently this only applies to `css_template`.

### Manual download

- Download the master.zip into your PicoCMS install's `/plugin` directory.
- Extract it in place - you should now have a `PicoGeSHi` folder that immediately contains `PicoGeSHi.php` and some other files. In other words, from your PicoCMS install's base directory, you should have `/plugins/PicoGeSHi/PicoGeSHi.php` (and some other files and directories).
- Change into the `/plugins/PicoGeSHi` directory and delete the empty `geshi-1.0` folder.
- Download & unzip [this GeSHi release][g1091] so that you now have a `geshi-1.0` folder again. Make sure your `/plugins/PicoGeshi` folder now contains `geshi-1.0/src/geshi.php` and related files.

That should be enough.

As usual, you should copy the `picogeshi.yml` configuration file to your [PicoCMS][p] installation's `/config` folder and make your adjustments there.

## Usage

You have a markdown article with a fenced code block. If you append a language keyword to the opening fence, this plugin will become active.
Example:

    ## This is what I coded in Lua:

    ~~~lua
    -- try to find themes in $HOME
    themedir = os.getenv("HOME") .. '/.local/share/themes/'
    ~~~

will use Lua syntax highlighting for this block of code.

[PicoCMS][p] uses Parsedown and [Parsedown Extra][pe] which is supposed to implement [Markdown Extra][me], but I found that this particular feature is not as rich as [outlined here][mef]:

- It won't handle more than one class added.
- If you use anything but the simple syntax above (e.g. `~~~ .lua` or `~~~{.lua}`) the extra characters show up in the class, e.g.: `<code class="language-{.lua">`.

Strangely, [GeSHi][g] can handle this, but the plugin sanitizes these strings nevertheless.

Even Markdown Extra has no means to put a CSS class on inline code, but you can write it out as HTML. Instead of

    I keyed in `while true; do echo Yes; done` without looking.

you have to write

    I keyed in <code class="language-bash">while true; do echo Yes; done</code> without looking.

## Colors and configuration

Please read `picogeshi.yml`  for more information.

## Languages

GeSHi currently supports the following language definitions:

- 4cs, 6502acme, 6502kickass, 6502tasm, 68000devpac
- abap, actionscript3, actionscript, ada, aimms, algol68, apache, applescript, apt_sources, arm, asm, asp, asymptote, autoconf, autohotkey, autoit, avisynth, awk
- bascomavr, bash, basic4gl, batch, bf, biblatex, bibtex, blitzbasic, bnf, boo
- caddcl, cadlisp, ceylon, cfdg, cfm, chaiscript, chapel, cil, c_loadrunner, clojure, c_mac, cmake, cobol, coffeescript, c, cpp, cpp-qt, cpp-winapi, csharp, css, cuesheet, c_winapi
- dart, dcl, dcpu16, dcs, delphi, diff, div, dos, dot, d
- ecmascript, eiffel, email, epc, e, erlang, euphoria, ezt
- f1, falcon, fo, fortran, freebasic, freeswitch, fsharp
- gambas, gdb, genero, genie, gettext, glsl, gml, gnuplot, go, groovy, gwbasic
- haskell, haxe, hicest, hq9plus, html4strict, html5
- icon, idl, ini, inno, intercal, io, ispfpanel
- java5, java, javascript, jcl, j, jquery, julia
- kixtart, klonec, klonecpp, kotlin, latex
- lb, ldif, lisp, llvm, locobasic, logtalk, lolcode, lotusformulas, lotusscript, lscript, lsl2, lua
- m68k, magiksf, make, mapbasic, mathematica, matlab, mercury, metapost, mirc, mk-61, mmix, modula2, modula3, mpasm, mxml, mysql
- nagios, netrexx, newlisp, nginx, nimrod, nsis
- oberon2, objc, objeck, ocaml-brief, ocaml, octave, oobas, oorexx, oracle11, oracle8, oxygene, oz
- parasail, parigp, pascal, pcre, perl6, perl, per, pf, phix, php-brief, php, pic16, pike, pixelbender, pli, plsql, postgresql, postscript, povray, powerbuilder, powershell, proftpd, progress, prolog, properties, providex, purebasic, pycon, pys60, python
- qbasic, qml, q
- racket, rails, rbs, rebol, reg, rexx, robots, roff, rpmspec, rsplus, ruby, rust
- sas, sass, scala, scheme, scilab, scl, sdlbasic, smalltalk, smarty, spark, sparql, sql, sshconfig, standardml, stonescript, swift, systemverilog
- tclegg, tcl, teraterm, texgraph, text, thinbasic, tsql, twig, typoscript
- unicon, upc, urbi, uscript
- vala, vbnet, vb, vbscript, vedit, verilog, vhdl, vim, visualfoxpro, visualprolog
- whitespace, whois, winbatch, wolfram
- xbasic, xml, xojo, xorg_conf, xpp
- yaml
- z80, zxbasic

## Style sheets

[GeSHi][g] has a mechanism built in to create stylesheets on the fly, but this plugin is not using it.
GeSHi's styling is not suitable to adapt syntax color schemes to the overall theme and is generally way too complex imho.
It wasn't easy to come up with a blueprint stylesheet (`default.template.css`), but in the end it's quite short and easy to apply and adapt.

## Links

Get this repository here:

- <https://framagit.org/ohnonot/PicoGeSHi>
- <https://notabug.org/ohnonot/PicoGeSHi>

[p]: http://picocms.org/
[g]: https://github.com/GeSHi/geshi-1.0
[pe]: https://github.com/erusev/parsedown-extra/
[me]: https://michelf.ca/projects/php-markdown/extra/
[mef]: https://michelf.ca/projects/php-markdown/extra/#fenced-code-blocks
[g1091]: https://github.com/GeSHi/geshi-1.0/releases/tag/v1.0.9.1
[pypi1]: https://notabug.org/ohnonot/PicoPygments
[pypi2]: https://framagit.org/ohnonot/PicoPygments