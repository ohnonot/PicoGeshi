<?php
error_reporting(0);
/**
 * Automatically build CSS stylesheet definitions from GeSHi files
 *
 * @author Andreas Gohr <andi@splitbrain.org>
 */

// fix the directory if needed:
$files = glob('geshi-1.0/src/geshi/*.php');


fwrite(STDERR,"step 1: finding style usage counts...\n");
$counts = array();
foreach($files as $file){
    include($file);
    foreach($language_data['STYLES'] as $type => $styles){
        foreach($styles as $style => $css){
            $counts[$type.$style][$css] += 1;
        }
    }
}
print_r($counts);

fwrite(STDERR,"step2: finding most common styles...\n");
$common = array();
foreach($counts as $type => $styles){
    asort($styles);
    $style = array_pop(array_keys($styles));
    $count = array_pop(array_values($styles));

    if($count > 1 && !empty($style)){
        $common[$type] = $style;
    }
}
ksort($common);

fwrite(STDERR,"step3: finding styles different fromt the common ones...\n");
$diff = array();
foreach($files as $file){
    include($file);
    $lang = basename($file,'.php');

    foreach($language_data['STYLES'] as $type => $styles){
        foreach($styles as $style => $css){
            if(!empty($css) && $common[$type.$style] != $css){
                $diff[$lang][$type.$style] = $css;
            }
        }
        if(is_array($diff[$lang])) ksort($diff[$lang]);
    }
}
ksort($diff);


fwrite(STDERR,"step4: creating CSS styles...\n\n");

echo "/* Common Code highlighting styles */\n\n";
foreach($common as $name => $style){
    echo '.code .'.classname($name).' { '.$style.' }'."\n";
}

echo "\n\n/* additional language styles */\n\n";
foreach($diff as $lang => $styles){
    echo "\n/* $lang overrides */\n";
    foreach($styles as $name => $style){
        echo '.code.'.$lang.' .'.classname($name).' { '.$style.' }'."\n";
    }
}


// helper to build the short class names
function classname($name){
    $name = str_replace('KEYWORDS','kw',$name);
    $name = str_replace('COMMENTSHARD','co_h',$name);
    $name = str_replace('COMMENTS','co',$name);
    $name = str_replace('BRACKETS','br',$name);
    $name = str_replace('ESCAPE_CHAR','es',$name);
    $name = str_replace('STRINGSHARD','st_h',$name);
    $name = str_replace('STRINGS','st',$name);
    $name = str_replace('NUMBERS','nu',$name);
    $name = str_replace('METHODS','me',$name);
    $name = str_replace('SYMBOLS','sy',$name);
    $name = str_replace('REGEXPS','re',$name);
    $name = str_replace('SCRIPT','sc',$name);
    return $name;
}